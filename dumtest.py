import unittest, csv

def group(lines):
	grouped = {}
	for curLine in lines:
		key = curLine[1]
		if (key not in grouped):
			grouped[key] = list()
		grouped[key].append(curLine)
	return grouped

class MyTest(unittest.TestCase):
    def test(self):
		with open('somefile.csv', 'rb') as csvfile:
			reader = csv.reader(csvfile)
			grouped = group( list(reader) )

			self.assertEquals(len(grouped), 2)
			self.assertTrue("COFFEE" in grouped)
			self.assertTrue("HOT CHOCOLATE" in grouped)